## Attention, not tested on large amount of files!

 * 1. Only analyzes the files in the given dir and creates a output file which maps the analyzed files to their file type 
 * - java -jar SortMyFiles -a [pathToDir] [pathToOutputFile] 
 * 
 * 2. Reads an analyzed mapping file and sorts the listed files with the sorter to the given output directory. 
 * - java -jar SortMyFiles -fs [pathToAnalyzedMappingFile] [pathToOutputDir] 
 * 
 * 3. Analyzes and sorts all files in the given directory. Sorted files are located in the given output dir. 
 * - java -jar SortMyFiles -s [pathToDir] [pathToOutputDir