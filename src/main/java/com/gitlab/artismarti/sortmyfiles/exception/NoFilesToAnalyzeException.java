package com.gitlab.artismarti.sortmyfiles.exception;

/**
 * @author artur
 */
public class NoFilesToAnalyzeException extends Throwable {

	public NoFilesToAnalyzeException(String message) {
		super(message);
	}

}
