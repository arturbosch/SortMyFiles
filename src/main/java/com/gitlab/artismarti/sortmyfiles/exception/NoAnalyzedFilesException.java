package com.gitlab.artismarti.sortmyfiles.exception;

/**
 * @author artur
 */
public class NoAnalyzedFilesException extends Exception {

	public NoAnalyzedFilesException(String message) {
		super(message);
	}
}
