package com.gitlab.artismarti.sortmyfiles;

import com.gitlab.artismarti.sortmyfiles.core.Analyzer;
import com.gitlab.artismarti.sortmyfiles.core.Sorter;
import com.gitlab.artismarti.sortmyfiles.exception.NoAnalyzedFilesException;
import com.gitlab.artismarti.sortmyfiles.exception.NoFilesToAnalyzeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author artur
 */
public class App {

	private static final String USAGE =
		" * Usage:\n * <p>\n * 1. Only analyzes the files in the given dir and\n"
			+ " * creates a output file which maps the analyzed files to their file type\n "
			+ "* - java -jar SortMyFiles -a [pathToDir] [pathToOutputFile]\n * <p>\n "
			+ "* 2. Reads an analyzed mapping file and sorts the listed files with the sorter to the given\n "
			+ "* output directory.\n * - java -jar SortMyFiles -fs [pathToAnalyzedMappingFile] [pathToOutputDir]\n "
			+ "* <p>\n * 3. Analyzes and sorts all files in the given directory. Sorted files are located in the\n "
			+ "* given output dir.\n * - java -jar SortMyFiles -s [pathToDir] [pathToOutputDir]\n * <p>";

	public static void main(String[] args) throws NoFilesToAnalyzeException, IOException, NoAnalyzedFilesException {
		ArrayList<String> argList = new ArrayList<>(Arrays.asList(args));

		try {
			if (argList.contains("-a")) {
				argList.remove("-a");
				Analyzer.newFinishedAnalyzer(argList.get(0), argList.get(1));
			} else {
				Analyzer analyzer;
				if (argList.contains("-fs")) {
					argList.remove("-fs");
					analyzer = Analyzer.newFinishedAnalyzerFromOutputFile(argList.get(0));
					(new Sorter(argList.get(1))).startSort(analyzer);
				} else if (argList.contains("-s")) {
					argList.remove("-s");
					analyzer = new Analyzer();
					analyzer.analyzeFiles(argList.get(0));
					(new Sorter(argList.get(1))).startSort(analyzer);
				} else {
					System.out.println(USAGE);
				}
			}
		} catch (IndexOutOfBoundsException e) {
			System.out.println(USAGE);
		}
	}

}
