package com.gitlab.artismarti.sortmyfiles.core;

/**
 * @author artur
 */
public enum FileType {

	music(new String[] {"wav", "mp3", "wma", "acc", "ogg", "flac", "rm"}, "/music"),
	text(new String[] {"txt", "csv", "rtf", "tex"}, "/text"),
	document(new String[] {"doc", "docx", "dot", "odt", "ods", "odp", "wps", "wpt", "wdb", "xls", "xlsx", "ppt", "pptx"}, "/document"),
	article(new String[] {"pdf"}, "/article"),
	video(new String[] {"wmv", "mpg", "avi", "mov", "flv", "mp4"}, "/video"),
	program(new String[] {"java", "cpp", "py", "groovy", "ada", "class"}, "/program"),
	unknown(new String[0], "/unknown"),
	picture(new String[] {"png", "jpg", "jpeg", "jpe", "bmp", "dib", "gif", "tiff", "tga", "psd", "ai", "odi"}, "/picture"),
	internet(new String[] {"htm", "html", "xml", "php", "asp", "css"}, "/internet");

	private String[] types;
	private String dir;

	FileType(String[] types, String dir) {
		this.types = types;
		this.dir = dir;
	}

	public String[] getTypes() {
		return this.types;
	}

	public String getDir() {
		return this.dir;
	}
}
