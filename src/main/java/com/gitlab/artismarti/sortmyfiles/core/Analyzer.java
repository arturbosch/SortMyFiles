package com.gitlab.artismarti.sortmyfiles.core;

import com.gitlab.artismarti.sortmyfiles.exception.NoFilesToAnalyzeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;

/**
 * @author artur
 */
public class Analyzer implements IAnalyzer {

	private static final Logger LOGGER = LoggerFactory.getLogger(Analyzer.class);

	private final HashMap<String, FileType> fileToTypeMap = new HashMap<>();

	public static Analyzer newFinishedAnalyzer(String pathToDir, String outputFile)
		throws NoFilesToAnalyzeException {
		Analyzer analyzer = new Analyzer();
		analyzer.analyzeFiles(pathToDir);
		analyzer.writeLexikonFile(outputFile);
		return analyzer;
	}

	public static Analyzer newFinishedAnalyzerFromOutputFile(String pathToOutputFile)
		throws IOException, NoFilesToAnalyzeException {
		Analyzer analyzer = new Analyzer();
		analyzer.readFromLexikonFile(pathToOutputFile);
		return analyzer;
	}

	@Override
	public void analyzeFiles(String files) throws NoFilesToAnalyzeException {
		Path startPath = Paths.get(files);
		if (Files.notExists(startPath)) {
			throw new NoFilesToAnalyzeException("There are no files to analyze.");
		} else {
			this.analyzeFiles(startPath);
		}
	}

	private void analyzeFiles(Path dir) {
		try {
			Files.list(dir).forEach((path) -> {
				if (Files.isDirectory(path)) {
					this.analyzeFiles(path);
				} else {
					this.analyzeType(path);
				}

			});
		} catch (IOException e) {
			LOGGER.error("Provided file is not a directory!");
		}

	}

	private void analyzeType(Path path) {
		if (Files.exists(path)) {
			FileType type = this.getType(path);
			this.fileToTypeMap.put(path.toAbsolutePath().toString(), type);
		}

	}

	private FileType getType(Path path) {
		FileType[] fileTypes = FileType.values();

		for (FileType fileType : fileTypes) {
			String[] types = fileType.getTypes();

			for (String type : types) {
				if (path.toString().toLowerCase().endsWith(type)) {
					return fileType;
				}
			}
		}

		return FileType.unknown;
	}

	@Override
	public void writeLexikonFile(String outputFile) {
		Path outputPath = Paths.get(outputFile);

		try {
			Files.createFile(outputPath);

			try (BufferedWriter writer = Files.newBufferedWriter(outputPath)) {
				this.fileToTypeMap.entrySet().stream().forEach((entry) -> {
					String entryAsLine = entry.getKey() + ";" + entry.getValue();

					try {
						writer.write(entryAsLine);
						writer.newLine();
					} catch (IOException e) {
						LOGGER.error("Could not write " + entryAsLine + " into output file!");
					}

				});
			}
		} catch (IOException e) {
			LOGGER.error("Could not create output analyze file!");
		}

	}

	@Override
	public void readFromLexikonFile(String pathToOutputFile) throws NoFilesToAnalyzeException, IOException {
		Path path = Paths.get(pathToOutputFile);

		if (!Files.isDirectory(path) && Files.exists(path)) {

			Files.readAllLines(path).forEach(
				line -> {
					String[] words = line.split(";");

					try {
						this.fileToTypeMap.put(words[0], FileType.valueOf(words[1]));
					} catch (ArrayIndexOutOfBoundsException e) {
						LOGGER.error("Current line: " + Arrays.toString(words) + " is invalid.");
					}

				});
		} else {
			throw new NoFilesToAnalyzeException("There are no files to analyze.");
		}
	}

	public HashMap<String, FileType> getFileToTypeMap() {
		return this.fileToTypeMap;
	}
}
