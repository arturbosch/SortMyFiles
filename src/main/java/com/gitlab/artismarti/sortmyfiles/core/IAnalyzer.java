package com.gitlab.artismarti.sortmyfiles.core;

import com.gitlab.artismarti.sortmyfiles.exception.NoFilesToAnalyzeException;

import java.io.IOException;
import java.util.HashMap;

/**
 * @author artur
 */
interface IAnalyzer {

	void analyzeFiles(String files) throws NoFilesToAnalyzeException;

	void writeLexikonFile(String outputFile);

	HashMap<String, FileType> getFileToTypeMap();

	void readFromLexikonFile(String pathToOutputFile) throws NoFilesToAnalyzeException, IOException;
}
