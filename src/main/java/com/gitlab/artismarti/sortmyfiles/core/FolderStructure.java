package com.gitlab.artismarti.sortmyfiles.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * @author artur
 */
class FolderStructure {

	private static final Logger LOGGER = LoggerFactory.getLogger(FolderStructure.class);

	private static final String MUSIC_FOLDER = "/music";
	private static final String TEXT_FOLDER = "/text";
	private static final String DOCUMENT_FOLDER = "/document";
	private static final String ARTICLE_FOLDER = "/article";
	private static final String VIDEO_FOLDER = "/video";
	private static final String PROGRAM_FOLDER = "/program";
	private static final String PICTURE_FOLDER = "/picture";
	private static final String INTERNET_FOLDER = "/internet";
	private static final String UNKNOWN_FOLDER = "/unknown";

	private static final List<String> FOLDERS = Arrays.asList(MUSIC_FOLDER, TEXT_FOLDER, DOCUMENT_FOLDER,
		ARTICLE_FOLDER, VIDEO_FOLDER, PROGRAM_FOLDER, PICTURE_FOLDER, INTERNET_FOLDER, UNKNOWN_FOLDER);

	private FolderStructure() {
	}

	static boolean createOutputDirs(String outputDir) {
		if (!checkOutputDir(outputDir)) {
			return false;
		} else {

			for (String path : FOLDERS) {
				try {
					Path e = Paths.get(outputDir, path);
					if (!Files.exists(e)) {
						Files.createDirectory(e);
					}
				} catch (IOException var4) {
					LOGGER.error("Could not create output dirs, especially: " + path, var4);
					return false;
				}
			}

			return true;
		}
	}

	private static boolean checkOutputDir(String outputDir) {
		Path checkDir = Paths.get(outputDir);
		if (!Files.isDirectory(checkDir)) {
			if (Files.exists(checkDir)) {
				return false;
			}

			try {
				Files.createDirectory(checkDir);
			} catch (IOException var3) {
				LOGGER.error("Could not create output directory");
				return false;
			}
		}

		return true;
	}
}
