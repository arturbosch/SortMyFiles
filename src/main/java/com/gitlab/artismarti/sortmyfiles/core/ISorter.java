package com.gitlab.artismarti.sortmyfiles.core;

import com.gitlab.artismarti.sortmyfiles.exception.NoAnalyzedFilesException;

/**
 * @author artur
 */
interface ISorter {

	void startSort(IAnalyzer analyzer) throws NoAnalyzedFilesException;
}
