package com.gitlab.artismarti.sortmyfiles.core;

import com.gitlab.artismarti.sortmyfiles.exception.NoAnalyzedFilesException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

/**
 * @author artur
 */
public class Sorter implements ISorter {

	private static final Logger LOGGER = LoggerFactory.getLogger(Sorter.class);
	private final String startFolder;

	public Sorter(String folderForResults) {
		this.startFolder = folderForResults;
		FolderStructure.createOutputDirs(this.startFolder);
	}

	@Override
	public void startSort(IAnalyzer analyzer) throws NoAnalyzedFilesException {
		HashMap<String, FileType> fileToTypeMap = analyzer.getFileToTypeMap();
		fileToTypeMap.entrySet().forEach(entry -> this.moveFile(entry.getKey(), entry.getValue()));
	}

	private void moveFile(String pathString, FileType type) {
		String fileName = Paths.get(pathString).getFileName().toString();

		try {
			Files.move(Paths.get(pathString), Paths.get(this.startFolder, type.getDir(), fileName));
		} catch (IOException e) {
			LOGGER.error("Could not move " + pathString + " to new destination.", e);
		}

	}

}
