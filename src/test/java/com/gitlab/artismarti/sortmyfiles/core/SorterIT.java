package com.gitlab.artismarti.sortmyfiles.core;

import com.gitlab.artismarti.sortmyfiles.exception.NoAnalyzedFilesException;
import com.gitlab.artismarti.sortmyfiles.exception.NoFilesToAnalyzeException;
import org.junit.Test;

/**
 * @author artur
 */
public class SorterIT {

	@Test
	public void startSort() throws NoFilesToAnalyzeException, NoAnalyzedFilesException {
		Analyzer analyzer = new Analyzer();
		analyzer.analyzeFiles("./files");
		new Sorter("./output").startSort(analyzer);
	}
}
