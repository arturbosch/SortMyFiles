package com.gitlab.artismarti.sortmyfiles.core;

import com.gitlab.artismarti.sortmyfiles.exception.NoFilesToAnalyzeException;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

/**
 * @author artur
 */
public class AnalyzerTest {

	private static final String FILES = "./files";
	private static final String OUTPUT_WRITE_TO = "./output.txt";
	private static final String OUTPUT_READ_FROM = "./output2.txt";

	@Test
	public void readFilesAndGenerateAssignment() throws NoFilesToAnalyzeException {
		IAnalyzer analyzer = new Analyzer();
		analyzer.analyzeFiles(FILES);
		assertAnalyzedMapIsCorrect(analyzer);
	}

	private void assertAnalyzedMapIsCorrect(IAnalyzer analyzer) {
		HashMap<String, FileType> fileToTypeMap = analyzer.getFileToTypeMap();
		assertThat(fileToTypeMap.size(), greaterThanOrEqualTo(4));
		assertFileType(fileToTypeMap, FileType.text);
	}

	private void assertFileType(HashMap<String, FileType> fileToTypeMap, FileType expectedType) {
		fileToTypeMap.entrySet()
			.stream()
			.map(Map.Entry::getValue)
			.forEach(type -> assertThat(type, is(expectedType)));
	}

	@Test
	public void getFinishedAnalyzerAndLookForOutputFile() throws NoFilesToAnalyzeException, IOException {
		Analyzer.newFinishedAnalyzer(FILES, OUTPUT_WRITE_TO);
		Path testPath = Paths.get(OUTPUT_WRITE_TO);

		assertThat(Files.exists(testPath), is(true));
		assertThat(Files.deleteIfExists(testPath), is(true));
	}

	@Test
	public void getFinishedAnalyzerFromOutputFile() throws IOException, NoFilesToAnalyzeException {
		IAnalyzer analyzer = Analyzer.newFinishedAnalyzerFromOutputFile(OUTPUT_READ_FROM);
		assertAnalyzedMapIsCorrect(analyzer);
	}
}
